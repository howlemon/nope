var roomID;
var peer;
createNewPeer();
function startID() {
    if (peer) {
        peer.destroy();
    }else{
        createNewPeer();
    }
    
}

function joinRoom() {
    roomID = $("#peer-id").val();
    createConnection(roomID);
}

function createMsg(input, type) {
    $("#msgbox").append(`<span class="${type}">${input}</span>`)
    $('#msgbox').animate({
        scrollTop: $('#msgbox')[0].scrollHeight
    }, "slow");
}

$(document).ready(function () {
    $("#input").keypress(function (e) {
        code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            createMsg($("#input").val(), "self")
            conn.send($("#input").val());
            $("#input").val("");
            
        }

    })
});

function createNewPeer(){
    peer = new Peer();
    peer.on('open', function (roomID) {
        console.log('My peer ID is: ' + roomID);
        $("#your-id").html(roomID);
        createMsg(`Your ID: ${roomID}`);
    });

    peer.on('close', function () {
        createMsg(`Room Disconnected`);
        $("#your-id").html("Not connected yet");
    });
    peer.on('connection', function (newConn) {
        createMsg(`User Connected: ${newConn.peer}`);
        newConn.on("data",function(msg){
            createMsg(msg,"other");
        });
        conn = newConn;
        console.log(newConn)
    });
    peer.on('data',function (msg){
        createMsg(msg,"other");
        console.log(msg);
    })
    peer.on('error',function (err){
        createMsg(err);
        console.log(err);
    })
}
//others
function createConnection(roomID){
    conn = peer.connect(roomID);
    conn.on('open', function() {
        // Receive messages
        conn.on('data', function(data) {
          console.log('Received', data);
        });
      
        // Send messages
        createMsg(`Room Connected:` + roomID);
        conn.send('Hello!');
    });
    conn.on('data', function (msg) {
        createMsg(msg,"other");
    });
    conn.on('close', function () {
        createMsg(`User Disconnected`);
        $("#your-id").html("Not connected yet");
    });
}